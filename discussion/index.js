let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
let tasks = [
'shower',
'eat breakfast',
'go to work',
'go home',
'go to sleep'
];

/*Getting an element's index*/
/*
console.log(grades.indexOf(89.2));


grades[grades.length] = 96;*/

/*grades.push(95, 100)*/

// console.log(grades);


/*Array Methods

push() - adds an element at the end of an array and returns the array's length


*/

/*grades.push(88.8); 
console.log(grades);*/

/*console.log(grades.push(88.8))*/; /*Returns the array's new length*/

//.pop() - removes an array's last element and returns the removed element.

/*grades.pop();
console.log(grades);
grades.push(grades.pop())*/

/*let classA = ["John", "Jacob", "Jack"];
let classB = ["Bob", "Bill", "Ben"];

classB.push(classA.pop());
console.log(classB); */
//adds "Jack to the last index of classB"

//.unishift() - adds an element to the beginning of an array and returns the array's new length

/*grades.unshift(77.7);
console.log(grades);*/

// .shift() - removes an array's first element and returns the array's new length

/*grades.shift(80)
console.log(grades)*/

//.splice() - can add and remove from anywhere in an array, and even do both at once.

//syntax: .splice(starting index number, number of items to remove, items to add <optional>)

/*console.log(tasks);*/

// Add with splice

/*tasks.splice(3, 0, "eat lunch");*/
//3 what index number to start, 0 number of items to remove, "eat lunch" item to be added at index[3]
/*console.log(tasks);*/

// Remove with splice

/*tasks.splice(2, 1);
console.log(tasks);*/

//add and remove with splice
/*tasks.splice(3, 1, "code");
console.log(tasks);*/

// .sort() - rearranges array elements in alphanumeric
/*computerBrands.sort();
console.log(computerBrands);
*/
// .reverse() - reverses the order of your array
/*computerBrands.reverse();
console.log(computerBrands);
*/

//.concat() - combines two arrays (the second array at the end of the first array)

/*let a = [1, 2, 3, 4, 5];
let b = [6, 7, 8, 9, 10];

let ab = a.concat(b);
console.log(ab);*/

// .join - converts the elements of an array into string
/*let joinedArray = ab.join(" and ")*/
//string " and " is now the separator instead of the default ","
/*console.log(joinedArray);*/

//.slice() - copies specified ranged of array elements into a new array
//syntax - slice(starting array, ending array)
/*let c = a.slice(0,3);*/
//0 - to start in index[0], to end in index[3]
//everything before the stopping point is copied
/*console.log(a);
console.log(c);*/

/*let numbersArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let placeholderArr = [];

console.log(numbersArr)
console.log(placeholderArr)


for(let i = 0; i < numbersArr.length; i++){
	if(
	numbersArr[i] % 2 === 0	//used to check if the number is even
		){
		placeholderArr.push(numbersArr[i])
	} 
}
console.log(placeholderArr)*/

// Array Iteration Methods

// .forEach() - used for array

/*computerBrands.forEach(function(brand){
	console.log(brand)
})
*//*
works exactly the same as:

for(let i = 0; i <computerBrands.length; i++){
	console.log(computerBrands[i])
}
*/

//.map() - creates a new array

let numbersArr= [1, 2, 3, 4, 5];

/*let doubledNumbers = numbersArr.map(function(number){
	return number * 2;
})
console.log(doubledNumbers)*/

// .every() - check every element in an array and see if ALL match your given condition

/*let allValid = numbersArr.every(function(number){
	return number <3;
})
console.log(allValid)

//.some() - check very element in an array and see if SOME match your given condition

let someValid = numbersArr.some(function(number){
	return number <3;
})
console.log(someValid)

//.filter() - creates a new array only populated with elements that match given condition

let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

let wordsResult = words.filter(function(word){
	return word.length > 6;
})
console.log(wordsResult);*/

//.includes() - checks if the specified value is included in the array (returns true or false)

//console.log(words.includes('limit'))

/*let twoDimensional = [
[1.1, 1.2, 1.3],
[2.1, 2.2, 2.3],
[3.1, 3.2, 3.3]
]

console.log(twoDimensional[0][1]) //[0]-row, [1]-column*/

