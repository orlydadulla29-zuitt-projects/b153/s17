let studentArr = [];

function addStudent(){	

	let name = prompt("Enter name: ");	
	studentArr.push(name);	
	console.log(name + " was added to the student's list.");
};

function countStudents(){

	console.log("There are a total of " + studentArr.length + " student(s) enrolled.");
};

function printStudents(){

	studentArr.sort();
	studentArr.forEach(function(students){
	console.log(students);
	});
}

function findStudent(){
	
	let findName = prompt("Find a name: ");
	let checkName1 = studentArr.includes(findName);

	let newStudentArr = studentArr.map(function(v){
	return v.toLowerCase()});

	let checkName2 = newStudentArr.includes(findName);

	if(checkName1 == true){
		index1 = studentArr.indexOf(findName);
		console.log(studentArr[index1] + " is an enrollee");
	} else if (checkName2 == true){
		index2 = newStudentArr.indexOf(findName);
		console.log(studentArr[index2] + " is an enrollee");
	}
	else {
		console.log("No student found with the name " + findName);
	}
};

function addSection(){

	let newStudentArr = studentArr.slice(0,studentArr.length);
	let section = prompt("Assign section: ");
	for (let index = 0; index < 1; index++) {	
		for (let count = 0; count < newStudentArr.length; count++){
		newStudentArr[count] = newStudentArr[count] + " - Section " + section
		}
	console.log(newStudentArr);
	}
};

function removeStudent(){

	let toRemove = prompt("Type the student's name to remove: ");
    for( let r = 0; r < studentArr.length; r++){ 
        if (
        	studentArr[r] === toRemove ||
        	studentArr[r].toLowerCase() === toRemove
           ) { 
        	studentArr.splice(r, 1); 
            console.log(toRemove + " was removed from student's list");
        } 
    }
};